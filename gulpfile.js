var gulp        =   require('gulp');

// Load plugins
var plugin = require('gulp-load-plugins')({
  pattern: '*'
});

var paths = {
  css:      'app/resources/css',
  sass:     'app/resources/scss',
  js:       'app/**/*.module.js, app/components/**/*.js',
  html:     'app/*.html'
};

//  Compile SASS using Compass,
//  Compass required as we using its utitlities
gulp.task('compass', function(){
  return gulp.src( paths.sass + '/*.scss' )
    .pipe(plugin.compass({
      css: paths.css,
      sass: paths.sass,
      task: 'watch',
      comments: false
    }));
});

// Watch task, keep checking for changes
gulp.task('watch', function(){
  gulp.watch([
    paths.sass + '/*.scss',
    paths.html,
    paths.js
  ]).on('change', plugin.browserSync.reload);
});

//  LiveReload Task
gulp.task('serve', function(){
    plugin.browserSync.init([], {
      server:  {
        baseDir: ['./', './app']
      }
  });
});

// Build HTML
gulp.task('html', function(){
  return gulp.src('app/*.html')
              .pipe(plugin.useref())
              .pipe(plugin.if('*.js', plugin.uglify()))
              .pipe(plugin.if('*.css', plugin.cleanCss()))
              .pipe(gulp.dest('dist'));
});

// Copy Images
gulp.task('images', function(){
  return gulp.src('app/resources/images/**/*')
            .pipe(gulp.dest('dist/resources/images/'));
});

// Copy Fonts
gulp.task('fonts', function(){
  return gulp.src('app/resources/fonts/*')
            .pipe(gulp.dest('dist/resources/fonts/'));
});

// Copy Templates
gulp.task('templates', function(){
  return gulp.src('app/components/**/*')
            .pipe(gulp.dest('dist/components/'));
});

// Clean
gulp.task('clean', function(){
  return gulp.src([
      'dist/base', 'dist/components', 'dist/resources'
    ], {
      read: false
    })
    .pipe( plugin.clean());
});

// Build
gulp.task('build', [ 'compass', 'images', 'fonts', 'templates', 'html', 'serve'])

// Default
gulp.task('default', ['clean'], function(){
    gulp.start('build');
});
